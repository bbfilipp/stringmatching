package StringMatching;

import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;

/**
 * Hello world!
 *
 */
public class MultipleThreadMatcher
{
    private String[] data = {"qwe","ewq","qwe","qwe1","a","a"};

    public void setData(String[] data) {
        this.data = data;
    }

    public ConcurrentMap<String, Set<Integer>> getOccurences() {
        return occurences;
    }

    private ConcurrentMap<String, Set<Integer>> occurences = new ConcurrentHashMap<String, Set<Integer> >();

    private void count(int startPos, int len){
        for(int i = startPos; (i<startPos+len)&&(i<data.length); i++){
            char[] s_array = data[i].toCharArray();
            Arrays.sort(s_array);
            String key = new String(s_array);
            int index = i;
            occurences.compute(key, (k, v) -> {
                Set <Integer> indexes = (v == null) ? new HashSet<Integer>(): v;
                indexes.add(index);
                return indexes;
            });
        }
    }

    public ConcurrentMap<String, Set<Integer>> process(String[] data){

        setData(data);
        int numOfAvailableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("numOfAvailableThreads = " + numOfAvailableProcessors);

        int lenOfStandartBlock = (data.length-1)/(numOfAvailableProcessors)+1;
        //int lenOfReminderBlock = lenOfStandartBlock+1;

        long start = System.currentTimeMillis();
        ExecutorService executorService = Executors.newFixedThreadPool(numOfAvailableProcessors);

        for(int i = 0 ; i<numOfAvailableProcessors; i++ ){
            int startPos = i*lenOfStandartBlock;
            executorService.execute(()->count(startPos, lenOfStandartBlock));
        }
        executorService.shutdown();
        try {
            if (!executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS)) {
                executorService.shutdownNow();
            }
        } catch (InterruptedException e) {
            executorService.shutdownNow();
        }
        occurences = occurences.entrySet().stream().filter( entry -> entry.getValue().size() > 1 ).collect(Collectors.toConcurrentMap(Map.Entry::getKey, Map.Entry::getValue));
        long end = System.currentTimeMillis();
        System.out.println((end-start)/1000.0 + " sec");

        System.out.println(occurences);
        return occurences;
    }

    public static void main( String[] args )
    {
        new MultipleThreadMatcher().process(new String[]{"qwe","ewq","qwe","qwe1","a","a"});

    }

}

