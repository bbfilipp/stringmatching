package StringMatching;

import java.util.*;

public class OneThreadMatcher {

    private static String[] data={"qwe","ewq","qwe","qwe1","a","a"};


    public static void main(String[] args) {


        Map<String, List<Integer>> pairs = new HashMap<String, List<Integer> >();

        for(int i = 0; i<data.length; i++){

            char[] s_array = data[i].toCharArray();
            Arrays.sort(s_array);

            String key = new String(s_array);

            if(pairs.containsKey( key)){
                pairs.get(key).add(i);
            }
            else {
                List <Integer> indexes = new ArrayList<Integer>();
                indexes.add(i);
                pairs.put(key, indexes);
            }

        }


        pairs.keySet().forEach(str -> {
            if (pairs.get(str).size() > 1)
                System.out.println(str+" "+pairs.get(str).toString());
        });

    }
}
